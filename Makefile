help:
	@echo "help    this help"
	@echo "build   assemble + test"

build:
	./gradlew build

.PHONY: \
	help \
	build \
