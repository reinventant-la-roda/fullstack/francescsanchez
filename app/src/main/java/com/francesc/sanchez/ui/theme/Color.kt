package com.francesc.sanchez.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val Encerts3 = Color.Blue
val Encerts4 = Color.Yellow
val Encerts5 = Color.Magenta
val Encerts6 = Color.Red

fun intersectionColor(intersection: Any): Color =
    when (intersection) {
        is Int -> intersectionColor(intersection)
        else -> Color.White
    }
fun intersectionColor(intersection: Int): Color =
    when (intersection) {
        3 -> Encerts3
        4 -> Encerts4
        5 -> Encerts5
        6 -> Encerts6
        else -> Color.White
    }
