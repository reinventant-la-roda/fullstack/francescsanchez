package com.francesc.sanchez.model

data class Lottery6(
    val v1: Int,
    val v2: Int,
    val v3: Int,
    val v4: Int,
    val v5: Int,
    val v6: Int,
) {

    infix fun intersection(other: Lottery6?): Int =
        other?.let { this.toSet() intersect it.toSet() }?.size ?: 0

    private fun toSet() = setOf(v1, v2, v3, v4, v5, v6)
}
