package com.francesc.sanchez

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.francesc.sanchez.model.Lottery6
import com.francesc.sanchez.show.ConfigShow
import com.francesc.sanchez.show.ShowHeader
import com.francesc.sanchez.show.ShowLazyColumnLottery
import com.francesc.sanchez.ui.theme.FrancescSanchezTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FrancescSanchezTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier,
                    color = MaterialTheme.colorScheme.background
                ) {
                    mainPreview()
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun mainPreview() {
    val lotteriesList =
        List(50) { indexList -> List(220) { generateLotteryNumber(indexList + it) } }
    val config = ConfigShow(
        filterMatch = false,
        check = Lottery6(100, 101, 102, 103, 104, 150)
    )
    Column {
        ShowHeader(lotteries = lotteriesList.flatten(), config = config)
        Spacer(modifier = Modifier.height(12.dp))
        ShowLazyColumnLottery(lotteriesList = lotteriesList, config = config)
    }
}

private fun generateLotteryNumber(index: Int) =
    Lottery6(index, index + 1, index + 2, index + 3, index + 4, index + 50)
