package com.francesc.sanchez.show

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.francesc.sanchez.model.Lottery6

@Composable
fun ShowLazyColumnLottery(
    lotteriesList: List<List<Lottery6>>,
    config: ConfigShow,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        itemsIndexed(lotteriesList) { i, item ->
            ShowLazyColumnLottery(index = 1 + i, lotteries = item, config = config)
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowEvery() {
    ShowLazyColumnLottery(
        lotteriesList = List(50) { indexList -> List(220) { generateLotteryNumber(indexList + it) } },
        config = ConfigShow(filterMatch = false, check = Lottery6(100, 101, 102, 103, 104, 105)),
    )
}

@Preview(showBackground = true)
@Composable
private fun ShowMatch() {
    ShowLazyColumnLottery(
        lotteriesList = List(50) { indexList -> List(220) { generateLotteryNumber(indexList + it) } },
        config = ConfigShow(filterMatch = true, check = Lottery6(100, 101, 102, 103, 104, 105)),
    )
}

private fun generateLotteryNumber(index: Int) =
    Lottery6(index, index + 1, index + 2, index + 3, index + 4, index + 50)
