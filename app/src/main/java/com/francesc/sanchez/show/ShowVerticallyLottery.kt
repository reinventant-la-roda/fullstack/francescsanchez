package com.francesc.sanchez.show

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.francesc.sanchez.model.Lottery6

@Composable
fun ShowVerticallyLottery(lottery: Lottery6, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        MyText(lottery.v1)
        MyText(lottery.v2)
        MyText(lottery.v3)
        MyText(lottery.v4)
        MyText(lottery.v5)
        MyText(lottery.v6)
    }
}

@Preview(showBackground = true)
@Composable
fun ShowEmptyVerticallyLottery(modifier: Modifier = Modifier) {
    ShowVerticallyLottery(lottery = Lottery6(-1, -1, -1, -1, -1, -1), modifier)
}

@Composable
private fun MyText(value: Int) {
    Text(
        text = value.toString(),
        color = Color.Black,
        modifier = Modifier.defaultMinSize(20.dp),
        textAlign = TextAlign.Center,
    )
}

@Preview(showBackground = true)
@Composable
private fun ShowLotteryPreview() {
    ShowVerticallyLottery(
        Lottery6(0, 2, 3, 4, 42, 69),
        Modifier
            .background(color = Color.Cyan)
            .border(1.dp, Color.Black)
            .padding(2.dp)
    )
}
@Preview(showBackground = true)
@Composable
private fun ShowLotteryPreview2() {
    ShowVerticallyLottery(
        Lottery6(0, 2, 3, 4, 420, 1070),
        Modifier
            .background(color = Color.White)
            .border(1.dp, Color.Black)
            .padding(2.dp)
    )
}
