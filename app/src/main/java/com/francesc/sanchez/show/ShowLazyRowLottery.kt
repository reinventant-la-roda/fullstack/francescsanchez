package com.francesc.sanchez.show

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.francesc.sanchez.model.Lottery6

@Composable
fun ShowLazyColumnLottery(
    index: Int,
    lotteries: List<Lottery6>,
    config: ConfigShow,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = index.toString(),
            Modifier
                .rotate(-50f)
                .padding(3.dp)
        )
        LazyRow {
            itemsIndexed(lotteries) { i, item ->
                if (!config.filterMatch || item intersection config.check >= 3)
                    ShowColumnIndexLotteryCheck(
                        index = i + 1,
                        lottery = item,
                        config.check,
                        Modifier
                            .defaultMinSize(minWidth = 10.dp)
                            .background(Color.White)
                    )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowFilterFalseWithCheck() {
    val lotteries = List(220) { index -> generateLotteryNumber(index + 42) }
    ShowLazyColumnLottery(
        index = 42,
        lotteries = lotteries,
        config = ConfigShow(
            filterMatch = false,
            check = Lottery6(70, 71, 72, 73, 74, 75),
        ),
    )
}

@Preview(showBackground = true)
@Composable
private fun ShowFilterFalseWithoutCheck() {
    val lotteries = List(220) { index -> generateLotteryNumber(index + 42) }
    ShowLazyColumnLottery(
        index = 42,
        lotteries = lotteries,
        config = ConfigShow(
            filterMatch = false,
            check = null,
        ),
    )
}

@Preview(showBackground = true)
@Composable
private fun ShowFilterTrueWithCheck() {
    val lotteries = List(220) { index -> generateLotteryNumber(index + 42) }
    ShowLazyColumnLottery(
        index = 42,
        lotteries = lotteries,
        config = ConfigShow(
            filterMatch = true,
            check = Lottery6(60, 61, 62, 63, 64, 65),
        ),
    )
}

@Preview(showBackground = true)
@Composable
private fun ShowFilterTrueWithoutCheck() {
    val lotteries = List(220) { index -> generateLotteryNumber(index + 42) }
    ShowLazyColumnLottery(
        index = 42,
        lotteries = lotteries,
        config = ConfigShow(
            filterMatch = true,
            check = null,
        ),
    )
}

private fun generateLotteryNumber(index: Int) =
    Lottery6(index, index - 40, index + 2, index + 3, index + 4, index + 50)
