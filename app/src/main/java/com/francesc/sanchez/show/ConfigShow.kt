package com.francesc.sanchez.show

import com.francesc.sanchez.model.Lottery6

data class ConfigShow(
    val filterMatch: Boolean = false,
    val check: Lottery6? = null
)
