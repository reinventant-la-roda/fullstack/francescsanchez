package com.francesc.sanchez.show

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.francesc.sanchez.model.Lottery6
import com.francesc.sanchez.ui.theme.intersectionColor

@Composable
fun ShowColumnIndexLotteryCheck(
    index: Int,
    lottery: Lottery6,
    check: Lottery6?,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .defaultMinSize(minWidth = 20.dp)
            .width(IntrinsicSize.Max)
            .background(Color.Black),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Divider()
        ShowIndex(index = index)
        Divider()
        ShowVerticallyLottery(
            lottery = lottery,
            modifier = Modifier
                .padding(horizontal = 1.dp)
                .background(Color.White)
        )
        Divider()
        Spacer(
            Modifier
                .height(10.dp)
                .fillMaxWidth()
                .background(Color.White)
        )
        Divider()
        ShowDistanceLottery(lottery, check)
        Divider()
    }
}

@Composable
private fun ShowIndex(index: Int, modifier: Modifier = Modifier) {
    Text(
        text = index.toString(),
        color = Color.Red,
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.Bold,
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 1.dp)
            .background(Color.White)
    )
}

@Composable
fun ShowDistanceLottery(lottery: Lottery6, check: Lottery6?) {
    val intersection = check?.let { lottery intersection it } ?: ""
    Text(
        text = intersection.toString(),
        textAlign = TextAlign.Center,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 1.dp)
            .background(intersectionColor(intersection))
    )
}

@Preview(showBackground = true)
@Composable
private fun ShowColumnLotteryPreview() {
    val check = Lottery6(1, 2, 3, 4, 5, 6)
    Row {
        ShowColumnIndexLotteryCheck(
            index = 3,
            check = null,
            lottery = Lottery6(2, 40, 42, 45, 46, 48)
        )
        ShowColumnIndexLotteryCheck(index = 4, check = null, lottery = Lottery6(2, 5, 6, 7, 8, 9))
        ShowColumnIndexLotteryCheck(
            index = 5,
            check = null,
            lottery = Lottery6(2, 405, 42, 45, 46, 48)
        )
        ShowColumnIndexLotteryCheck(
            index = 6,
            check = check,
            lottery = Lottery6(2, 405, 42, 45, 46, 48)
        )
        ShowColumnIndexLotteryCheck(
            index = 7,
            check = check,
            lottery = Lottery6(1, 2, 2, 45, 46, 48)
        )
        ShowColumnIndexLotteryCheck(
            index = 8,
            check = check,
            lottery = Lottery6(1, 2, 3, 45, 46, 48)
        )
        ShowColumnIndexLotteryCheck(
            index = 9,
            check = check,
            lottery = Lottery6(1, 2, 3, 4, 46, 48)
        )
        ShowColumnIndexLotteryCheck(
            index = 10,
            check = check,
            lottery = Lottery6(1, 2, 3, 4, 6, 48)
        )
        ShowColumnIndexLotteryCheck(index = 11, check = check, lottery = Lottery6(1, 2, 3, 4, 5, 6))
    }
}
