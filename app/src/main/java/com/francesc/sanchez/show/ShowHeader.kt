package com.francesc.sanchez.show

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.francesc.sanchez.model.Lottery6
import com.francesc.sanchez.ui.theme.intersectionColor

@Composable
fun ShowHeader(
    lotteries: List<Lottery6>,
    config: ConfigShow,
    modifier: Modifier = Modifier
) {
    Row(modifier) {
        ShowHorizontalCheck(config.check, Modifier.weight(10f))
        Spacer(modifier = Modifier.weight(1f))
        ShowResumeResult(key = 3, all = lotteries, config = config, Modifier.weight(3f))
        Spacer(modifier = Modifier.weight(1f))
        ShowResumeResult(key = 4, all = lotteries, config = config, Modifier.weight(3f))
        Spacer(modifier = Modifier.weight(1f))
        ShowResumeResult(key = 5, all = lotteries, config = config, Modifier.weight(3f))
        Spacer(modifier = Modifier.weight(1f))
        ShowResumeResult(key = 6, all = lotteries, config = config, Modifier.weight(3f))
    }
}

@Composable
private fun ShowHorizontalCheck(check: Lottery6?, modifier: Modifier = Modifier) {
    Row(
        modifier = modifier,
    ) {
        ShowCheckValue(check?.v1, Modifier.weight(1f))
        ShowCheckValue(check?.v2, Modifier.weight(1f))
        ShowCheckValue(check?.v3, Modifier.weight(1f))
        ShowCheckValue(check?.v4, Modifier.weight(1f))
        ShowCheckValue(check?.v5, Modifier.weight(1f))
        ShowCheckValue(check?.v6, Modifier.weight(1f))
    }
}

@Composable
private fun ShowCheckValue(value: Int?, modifier: Modifier = Modifier) {
    Text(
        text = value?.toString() ?: "",
        textAlign = TextAlign.Center,
        modifier = modifier.border(1.dp, Color.Black),
    )
}

@Composable
private fun ShowResumeResult(
    key: Int,
    all: List<Lottery6>,
    config: ConfigShow,
    modifier: Modifier = Modifier
) {
    Row(modifier = modifier) {
        Text(
            text = key.toString(),
            textAlign = TextAlign.Center,
            modifier = Modifier
                .weight(1f)
                .background(intersectionColor(key))
        )
        ShowCheckValue(
            value = all.filter { it intersection config.check == key }.size,
            Modifier.weight(2f)
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowHorizontalCheckPreview() {
    Column {
        ShowHorizontalCheck(Lottery6(1, 2, 3, 4, 5, 69))
        ShowHorizontalCheck(null)
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowResumeResultPreview() {
    val all = generateLottery()
    val config = ConfigShow(check = Lottery6(1, 2, 3, 4, 5, 6))

    Column {
        ShowResumeResult(key = 3, all = all, config = config)
        ShowResumeResult(key = 4, all = all, config = config)
        ShowResumeResult(key = 5, all = all, config = config)
        ShowResumeResult(key = 6, all = all, config = config)
        Divider()
        Divider()
        ShowResumeResult(key = 3, all = all, config = ConfigShow())
        ShowResumeResult(key = 4, all = all, config = ConfigShow())
        ShowResumeResult(key = 5, all = all, config = ConfigShow())
        ShowResumeResult(key = 6, all = all, config = ConfigShow())
    }
}

@Preview(showBackground = true)
@Composable
private fun ShowHeaderPreview() {
    Column {
        ShowHeader(
            lotteries = generateLottery(),
            config = ConfigShow(check = Lottery6(1, 2, 3, 4, 5, 6))
        )
        ShowHeader(
            lotteries = emptyList(),
            config = ConfigShow(check = Lottery6(1, 2, 3, 4, 5, 640))
        )
        ShowHeader(
            lotteries = generateLottery(),
            config = ConfigShow()
        )
    }
}

private fun generateLottery() = listOf(
    Lottery6(1, 2, 3, 4, 5, 6),
    Lottery6(1, 2, 3, 4, 5, 50),
    Lottery6(1, 2, 3, 4, 45, 50),
    Lottery6(1, 2, 3, 44, 46, 50),
    Lottery6(1, 2, 3, 44, 47, 50),
    Lottery6(1, 2, 3, 44, 48, 50),
    Lottery6(1, 2, 43, 44, 49, 50),
    Lottery6(1, 42, 43, 44, 49, 50),
    Lottery6(41, 42, 43, 44, 49, 50),
)
